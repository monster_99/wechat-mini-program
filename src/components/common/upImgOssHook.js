import generateMixed from '@/utils/generateMixed.js'
import dayjs from 'dayjs'

const getFileExt = (url) => url.split(".").pop()

export default async (tempFiles) => {
	return await Promise.all(tempFiles.map(item => {
		item.fileKey =
			`${dayjs().format('YYYY/MM/DD')}/${generateMixed(20)}.${getFileExt(item.tempFilePath)}`
		return new Promise((resolve, reject) => {
			wx.cloud.uploadFile({
				cloudPath: item.fileKey,
				filePath: item.tempFilePath,
				config: {
					env: import.meta.env.VITE_ENV_ID
				},
				success: (res) => {
					console.log('res', res)
					resolve({
						...res,
						...item
					})
				},
				fail: reject,
				complete: () => {}
			})
		}).catch(err => {
			console.error(err)
		})
	})).catch(err => {
		console.error(err)
	})
}