export const searchListMap = () => ([{
		text: '综合排序',
		searchId: 0,
		searchIndex: 0,
		children: [{
				id: 1,
				text: '综合排序',
				selected: true,
			},
			{
				id: 2,
				text: '最新发布',
				selected: false,
			},
			{
				id: 3,
				text: '离我最近',
				selected: false,
			}
		]
	},
	{
		text: '商品分类',
		searchId: 0,
		searchIndex: 0,
		children: [{
				id: 1,
				text: '全部商品',
				selected: true,
			},
			{
				id: 2,
				text: '珊瑚',
				selected: false,
			},
			{
				id: 3,
				text: '海水鱼',
				selected: false,
			},
			{
				id: 4,
				text: '设备/消耗品',
				selected: false,
			},
			{
				id: 5,
				text: '其他海水生物',
				selected: false,
			}
		]
	},
	{
		text: '交易类型',
		searchId: 0,
		searchIndex: 0,
		children: [{
				id: 1,
				text: '全部',
				selected: true,
			}, {
				id: 2,
				text: '出售',
				selected: false,
			},
			{
				id: 3,
				text: '求购',
				selected: false,
			},
			{
				id: 4,
				text: '赠送',
				selected: false,
			}
		]
	},
]);