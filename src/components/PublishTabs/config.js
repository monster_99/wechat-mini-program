import {
	computed,
} from "vue"

export default (modelValue) => {
	const isType0 = computed(() => {
		return modelValue.value.articleType === 1
	})
	const isExchangeType = computed(() => {
		return modelValue.value.articleType === 4
	})
	
	return {
		isType0,
		isExchangeType
	}
}
