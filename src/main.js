import { createSSRApp } from 'vue';
import App from './App';
import store from './store';

import useShare from '@/hooks/useShare';
import getFileIdToUrl from '@/utils/getFileIdToUrl.js'

import uviewPlus from 'uview-plus';
import 'uview-plus/index.scss';

// https://blog.csdn.net/qq_40591925/article/details/129991995
// import addInterceptor from '@/utils/addInterceptor.js';

export function createApp() {
  const app = createSSRApp(App);
  app.use(store);
  app.use(uviewPlus);
  app.config.globalProperties.env = import.meta.env;
  app.config.globalProperties.getFileIdToUrl = getFileIdToUrl

  app.mixin(useShare().mixins);

  uni.$zp = {
    config: {
      //配置分页默认pageSize为15
      'default-page-size': '10',
    },
  };

  // 无效，源码看了，垃圾
  // uni.$u.setConfig

  return {
    app,
  };
}
