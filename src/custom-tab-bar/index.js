const { tabList } = require('./tabList.js');

Component({
  data: {
    selected: 0,
    color: '#666666',
    selectedColor: '#36A1EE',
    list: tabList,
  },
  attached() {},
  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset;
      console.log(data);
      const url = data.path;
      if (url === '/pages/publish/index') {
        wx.navigateTo({
          url,
        });
      } else {
        wx.switchTab({
          url,
        });
        this.setData({
          selected: data.index,
        });
      }
    },
  },
});
