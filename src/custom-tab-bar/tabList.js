exports.tabList = [
  {
    pagePath: '/pages/index/index',
    iconPath: '/static/images/tabbar/arix.png',
    selectedIconPath: '/static/images/tabbar/arix-1.png',
    text: '首页',
  },
  {
    pagePath: '/pages/publish/index',
    iconPath: '/static/images/tabbar/ht.png',
    selectedIconPath: '/static/images/tabbar/ht-1.png',
    text: '扫码',
    isSpecial: true,
  },
  {
    pagePath: '/pages/my/index',
    iconPath: '/static/images/tabbar/usdt.png',
    selectedIconPath: '/static/images/tabbar/usdt-1.png',
    text: '我的',
  },
];
