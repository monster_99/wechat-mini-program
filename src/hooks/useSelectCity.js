import { cloneDeep } from 'lodash';
// 发布相关 api接口
import commonApi from '@/api/common';

let cityData = [];

export default () => {
  // 获取城市数据
  const getCityData = async () => {
    if (cityData.length) return cloneDeep(cityData);
    let res = await commonApi.getCityList();
    cityData = res.data;

    return cloneDeep(cityData);
  };

  return {
    getCityData,
  };
};
