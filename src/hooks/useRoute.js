import { onLoad } from '@dcloudio/uni-app';
import qs from 'qs';

export default () => {
  const getQuery = () => {
    return new Promise((resolve) => {
      onLoad((query) => {
        resolve(query);
      });
    });
  };

  const qsStringify = (queryObj) => {
    return qs.stringify(queryObj, { encode: false });
  };

  return {
    getQuery,
    qsStringify,
  };
};
