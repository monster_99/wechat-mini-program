import useRoute from '@/hooks/useRoute';

export default () => {
  const { qsStringify } = useRoute();
  const title = '越野e族';
  return {
    title,
    mixins: {
      data() {
        return {
          useShareQueryData: {},
        };
      },
      onLoad(query) {
        this.useShareQueryData = query;
      },
      onShareAppMessage() {
        const curPages = getCurrentPages();
        const nowPahe = curPages[curPages.length - 1];
        const shareQuery = {};
        if (this.useShareQueryData.id) {
          shareQuery.id = this.useShareQueryData.id;
        }
        if (this.useShareQueryData.type) {
          shareQuery.type = this.useShareQueryData.type;
        }
        return {
          title,
          path: `/${nowPahe.route}?${qsStringify(shareQuery)}`,
        };
      },
    },
  };
};
