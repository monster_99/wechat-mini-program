export default () => {
  const navigateTo = uni.navigateTo;
  const redirectTo = uni.redirectTo;
  const reLaunch = uni.reLaunch;
  const switchTab = uni.switchTab;
  const navigateBack = (opt) => {
    const curPages = getCurrentPages();
    if (curPages.length === 1) {
      reLaunch({
        url: '/pages/index/index',
      });
    } else {
      uni.navigateBack(opt);
    }
  };

  return {
    navigateTo,
    redirectTo,
    reLaunch,
    switchTab,
    navigateBack,
  };
};
