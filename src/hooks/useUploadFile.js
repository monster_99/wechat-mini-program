export default () => {
  // 上传图片
  const uploadImage = (num) => {
    return new Promise((resolve, reject) => {
      uni.chooseImage({
        count: num,
        sizeType: ["original", "compressed"],
        sourceType: ["album", "camera"],
        success: function (res) {
          const len = res.tempFiles.length;
          res.tempFiles = res.tempFiles.filter((item) => {
            const path = item.path;
            const formatImage = path.split(".")[path.split(".").length - 1]; // 图片格式
            return ["png", "jpg", "jpeg"].includes(formatImage);
          });

          if (len !== res.tempFiles.length) {
            uni.showToast({
              title: "请上传png、jpg、jpeg的图片",
              icon: "none",
              mask: false,
            });
            if (!res.tempFiles.length) reject();
          }

          resolve(res);
        },
      });
    });
  };
  // 上传视频
  const uploadVideo = () => {
    return new Promise((resolve) => {
      uni.chooseVideo({
        count: 1,
        sourceType: ["album", "camera"],
        compressed: true, // 压缩视频
        success: function (res) {
          resolve(res);
        },
      });
    });
  };

  return {
    uploadImage,
    uploadVideo,
  };
};
