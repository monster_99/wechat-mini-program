import { toRef } from 'vue';
import { useStore } from 'vuex';
import storeIndex from '@/store/index';

let isLoginStart = false;

export default () => {
  const store = useStore();

  const token = (() => {
    if (store) return toRef(store.state.user, 'token');
    else return {};
  })();

  // js中使用，这个方法
  const getToken = () => {
    return storeIndex.state.user.token;
  };

  // 登录调用
  const wxLogin = async () => {
    if (!!getToken()) return getToken();
    if (isLoginStart) {
      return new Promise((resolve) => {
        setTimeout(async () => {
          resolve(await wxLogin());
        }, 200);
      });
    }
    isLoginStart = true;
    await storeIndex.dispatch('user/getToken');
    isLoginStart = false;
    return getToken();
  };

  return { token: token, getToken, wxLogin };
};
