import { computed } from "vue";
import { useStore } from "vuex";

import userApi from "@/api/user.js";

export default () => {
  const store = useStore();
  const userInfo = computed(() => store.state.user.userInfo);

  const reloadUserInfo = async () => {
    return await store.dispatch("user/getUserInfo");
  };

  const uplodaUserInfo = (data) => {
    return userApi.updateUserByOpenid(data).then(async (res) => {
      if (res.code === 200) {
        await reloadUserInfo();
        uni.showToast({
          title: "修改成功",
          icon: "none",
          mask: true,
        });
      } else {
        uni.showToast({
          title: "修改失败",
          icon: "none",
          mask: true,
        });
      }
    });
  };

  return {
    userInfo,
    reloadUserInfo,
    uplodaUserInfo,
  };
};
