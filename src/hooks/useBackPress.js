import { onLoad } from '@dcloudio/uni-app';
import { ref, watch } from 'vue';

export default (message) => {
  const isBackAlert = ref(false);

  const showBackAlert = () => {
    wx.enableAlertBeforeUnload({
      message,
      success: () => {},
    });
  };
  const hideBackAlert = () => {
    wx.disableAlertBeforeUnload({
      success: () => {},
    });
  };
  onLoad(() => {
    hideBackAlert();
  });
  const changeBackFlag = (flag) => {
    if (flag === isBackAlert.value) return false;
    if (flag) {
      showBackAlert();
    } else {
      hideBackAlert();
    }
  };

  return {
    changeBackFlag,
  };
};
