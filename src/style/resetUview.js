import tailwindColors from '@/utils/tailwindColors';

export const buttonPrimary = {
  color: tailwindColors.primary,
  customStyle: {
    color: tailwindColors.black[1],
    'font-size': '28rpx',
    'font-weight': 500,
    height: '100%',
    display: 'flex',
    'align-items': 'center',
    'justify-content': 'center',
  },
};
