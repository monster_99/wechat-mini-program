import {
  call
} from "./config.js"

export default {
  gteUserByOpenid(data = {}) {
    return call({
      path: '/api/user-management/openId',
      method: 'GET',
      data
    })
  },
  updateUserByOpenid(data = {}) {
    return call({
      path: '/api/user-management/update-user',
      method: 'POST',
      data
    })
  },
  getUserIdByUserId(userId ) {
    return call({
      path: `/api/user-management/${userId}`,
      method: "GET",
    });
  }
}
