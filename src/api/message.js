import {
  call
} from "./config.js"

export default {
  getAllMessageByUserId(userId) {
    return call({
      path: `/api/user-message-management/all-message-for-user/${userId}`,
      method: "GET",
    });
  },
  setMessageRead(data) {
    return call({
      path: `/api/user-message-management/message-read`,
      method: "POST",
      data,
    });
  },
}
