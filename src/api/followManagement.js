import {call} from "./config.js";

import store from "@/store/index.js";

export default {
  // 通过userId获得这个user 所有关注的人（我的粉丝）
  getFollowingUser(userId=store.state.user.userInfo.userId) {
    return call({
      path: `/api/follow-pair-management/following-user/${userId}`,
      method: "GET",
    });
  },
  // 通过userId获得被这个user 所有关注的人（我关注的人）
  getFollowedUser(userId=store.state.user.userInfo.userId) {
    return call({
      path: `/api/follow-pair-management/followed-user/${userId}`,
      method: "GET",
    });
  },
  // 创建一个新的关注
  newFollowPair(followData) {
    return call({
      path: `/api/follow-pair-management/new-follow-pair`,
      method: "POST",
      data: followData,
    });
  },
// /api/follow-pair-management/followed-user-num/{userId}	查询一个user的粉丝数量
  getFollowedUserNum(userId) {
    return call({
      path: `/api/follow-pair-management/followed-user-num/${userId}`,
      method: "GET",
    });
  },
// /api/followed-user/{followedUser}/following-user/{followingUser}	查询一个followed user是否被这个这个following user 关注
  checkFollowedUser(followedUser, followingUser) {
    return call({
      path: `/api/followed-user/${followedUser}/following-user/${followingUser}`,
      method: "GET",
    });
  }
}
