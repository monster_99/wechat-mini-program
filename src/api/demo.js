import { call } from "./config.js"

export const demo2 = {
	postCount(data) {
		return call({
			path: '/api/count',
			method: 'POST',
			data
		})
	},
	getOpenid(data = {}) {
		return call({
			path: '/api/wx_openid',
			method: 'GET',
			data
		})
	},
	findTreasure(data = {}) {
		return call({
			path: '/api/find-treasure',
			method: 'GET',
			data
		})
	},
}
