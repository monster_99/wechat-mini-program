import { call } from "./config.js";

import store from "@/store/index.js";

export default {
  createArticle(data) {
    return call({
      path: "/api/article-management",
      method: "POST",
      data,
    });
  },
  gteArticleListToIndex(data = {}) {
    return call({
      path: "/api/article-management/recent-ten-articles",
      method: "GET",
      data,
    });
  },
  getArticleDetail(id) {
    return call({
      path: `/api/article-management/${id}`,
    });
  },
  gteArticleListToSearch(target) {
    return call({
      path: `/api/article-management/search-by-tile/${encodeURI(target)}`,
      method: "GET",
    });
  },
  gteCommentManagement(articleId) {
    return call({
      path: `/api/article-comment-management/get-all-comments-for-article/${articleId}`,
      method: "GET",
    });
  },
  createCommentManagement(data) {
    return call({
      path: `/api/article-comment-management/new-article-comment`,
      method: "POST",
      data,
    });
  },
  getAllMessage(userId=store.state.user.userInfo.userId) {
    return call({
      path: `/api/user-message-management/all-message-for-user/${userId}`,
      method: "GET",
    });
  },
  getArticleListByUserId(userId) {
    return call({
      path: `/api/article-management/by-user-id/${userId}`,
      method: "GET",
    });
  },
  hideArticle(articleId) {
    return call({
      path: `/api/article-management/hide-article`,
      method: "POST",
      data: {
        articleId,
      },
    });
  },
  showArticle(articleId) {
    return call({
      path: `/api/article-management/show-article`,
      method: "POST",
      data: {
        articleId,
      },
    });
  },
  // /api/article-management/num-by-user-id/{userId}	查询一个user发布帖子的数量
  getNumOfArticlesByUserId(userId) {
    return call({
      path: `/api/article-management/num-by-user-id/${userId}`,
      method: "GET",
    });
  },
};

