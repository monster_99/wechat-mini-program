import {call} from "./config.js";

import store from "@/store/index.js";

export default {
  // 创建一个交换微信请求
  createSwitchWechatRequest(data) {
    return call({
      path: "/api/switch-wechat-management/new-switch-wechat-request",
      method: "POST",
      data,
    });
  },
  // 通过一个交换微信请求
  approveSwitchWechatId(data) {
    return call({
      path: "/api/user-collection-management/approve-switch-wechat-id",
      method: "POST",
      data,
    });
  },
  // 获得所有和我相关的交换微信请求
  getAllSwitchWechatId(userId = store.state.user.userInfo.userId) {
    return call({
      path: `/api/switch-wechat-management/get-all-switch-wechat-id/${userId}`,
      method: "GET",
    });
  }
}
