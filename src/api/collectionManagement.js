import {call} from "./config.js";

import store from "@/store/index.js";

export default {
// 查看我的所有收藏
  getAllCollectionsForUser(userId=store.state.user.userInfo.userId) {
    return call({
      path: `/api/user-collection-management/all-collections-for-user/${userId}`,
      method: "GET",
    });
  },
  // 创建一个收藏
  createNewCollection(collection) {
    return call({
      path: "/api/user-collection-management/new-user-collection",
      method: "POST",
      data: collection,
    });
  },
// /api/user-collection-management/user-id/{userId}/article-id/{articleId}	查询一个article是否被这个user收藏
  checkArticleIsInCollection(userId, articleId) {
    return call({
      path: `/api/user-collection-management/user-id/${userId}/article-id/${articleId}`,
      method: "GET",
    });
  },
}
