/**
 * 按长度分割数组
 * @param {Array} arr 原数组
 * @param {Number} length 分割长度
 * @returns {Array} 新数组
 */
export default (arr, length) => {
  return arr.reduce((total, current, index) => {
    if (index % length) {
      total[total.length - 1].push(current)
    } else {
      total.push([current])
    }
    return total
  }, [])
}
