export default (message) => {
	const showBackAlert = () => {
		wx.enableAlertBeforeUnload({
			message,
			success: () => {}

		});
	}
	const hideBackAlert = () => {
		wx.disableAlertBeforeUnload({
			success: () => {}
		})
	}
	
	return {
		showBackAlert,
		hideBackAlert,
	}
}