export default (name, type = 'png') => {
  /**
   * 获取本地图
   * @param name // 文件名 如 home-bg
   * @param type // 文件类型 如 png jpg
   * @returns {*|string}
   */
  const path = `../static/images/${name}.${type}`;
  const modules = import.meta.glob(`../static/images/**`, { eager: true });
  return modules[path].default;
};
