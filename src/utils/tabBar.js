import { onShow } from '@dcloudio/uni-app';

const { tabList } = require('../custom-tab-bar/tabList');

/**
 * 修改tabbar的index，直接引入即可，自动判断当前tabbar的index
 * @returns {any}
 */
export const useChangeTabbar = function () {
  let useIndex = 0;
  const curPages = getCurrentPages();
  const nowPahe = curPages[curPages.length - 1];
  for (let index = 0; index < tabList.length; index++) {
    const item = tabList[index];
    if (item.pagePath === `/${nowPahe.route}`) {
      useIndex = index;
      break;
    }
  }
  onShow(() => {
    const curPages = getCurrentPages()[0];
    if (typeof curPages.getTabBar === 'function' && curPages.getTabBar()) {
      curPages.getTabBar().setData({
        selected: useIndex,
      });
    }
  });
};
