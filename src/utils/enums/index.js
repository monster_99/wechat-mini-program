export const sellType = [{
	name: '珊瑚',
	type: '0',
	disabled: false
}, {
	name: '海水鱼',
	type: '1',
	disabled: false
}, {
	name: '设备/消耗品',
	type: '2',
	disabled: false
}, {
	name: '其他海水生物',
	type: '3',
	disabled: false
}]

export const publishType = [{
		name: '出售',
		id: 1
	},
	{
		name: '赠送',
		id: 2
	},
	{
		name: '求购',
		id: 3
	},
	{
		name: '交流/求助',
		id: 4
	}
]

export const expressMailType = [{
	type: 1,
	name: '自提/闪送',
	disabled: false
}, {
	type: 2,
	name: '邮寄',
	disabled: false
}]