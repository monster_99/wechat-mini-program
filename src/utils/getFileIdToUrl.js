export default (path) => {
	return new Promise((resolve, reject) => {
		console.log(`${path} 更换临时url`)
		wx.cloud.getTempFileURL({
			fileList: [`${import.meta.env.VITE_OSS_BUCKET}${path}`], // 文件唯一标识符 cloudID, 可通过上传文件接口获取
			success: res => {
				console.log(`${path} 更换临时url成功：${res.fileList[0].tempFileURL}`)
				resolve(res.fileList[0].tempFileURL)
			},
			fail: (err) => {
				console.error(err)
				reject(err)
			}
		})
	})
}