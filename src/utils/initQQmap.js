import QQMapWX from '@/utils/qqmap-wx-jssdk.js';

let qqmapsdk = null;

const initSdk = () => {
  qqmapsdk = new QQMapWX({
    key: import.meta.env.VITE_QQMAP_SDK_KEY,
  });
};

export default () => {
  if (!qqmapsdk) initSdk();
  return qqmapsdk;
};
