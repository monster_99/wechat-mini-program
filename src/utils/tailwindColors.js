export default {
  white: '#fff',
  black: {
    0: '#000',
    1: '#231815',
    3: '#333',
    6: '#666',
    9: '#999',
    97: '#979797'
  },
  gray: {
    'f1': '#F1F1F1',
    'f5': '#F5F5F5',
    'fa': '#FAFAFA',
  },
  primary: '#FDD000',
};
