import userApi from "@/api/user.js";

export default {
  namespaced: true,
  state: {
    userInfo: {
      userId:"67aa5525-b3f2-437e-afad-657c1a8fae0f",
    }
  },
  actions: {
    async getUserInfo({ commit }) {
      const userRes = await userApi.gteUserByOpenid()
      const userInfo = userRes.data
      console.log('store userInfo', userInfo)
      commit('SET_USER_INFO', userInfo)
      return userInfo
    }
  },
  mutations: {
    SET_USER_INFO(state, data = {}) {
      state.userInfo = data || {}
    }
  }
}
