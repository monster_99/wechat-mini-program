export default {
  namespaced: true,
  state: {
    keyboardRes: {},
  },
  mutations: {
    SET_KEYBOARD_HEIGHT(state, data = {}) {
      state.keyboardRes = data;
    },
  },
};
