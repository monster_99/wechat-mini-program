export default {
	namespaced: true,
	state: {
		pageHide: false
	},
	mutations: {
		SET_PAGE_HIDE(state, data = false) {
			state.pageHide = data
		}
	}
}