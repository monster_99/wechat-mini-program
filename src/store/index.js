import { createStore } from 'vuex'
import page from './modules/page.js'
import position from './modules/position.js'
import user from './modules/user';
// 键盘事件，因为全局只能存在一个，所以放到store
import keyboard from './modules/keyboard.js';

const store = createStore({
	modules: {
		page,
		position,
		user,
		keyboard,
	}
})

export default store
