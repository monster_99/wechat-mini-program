export function buildTimePlugin(mode) {
  return {
    name: 'build-time',

    // 在 buildStart 阶段设置初始值
    buildStart() {
      this.startTime = Date.now();
      if (mode !== 'development') console.log('开始打包');
    },

    //  输出打包时间
    closeBundle(options, bundle) {
      const timeDiff = (Date.now() - this.startTime) / 1000;
      if (mode !== 'development') console.log(`打包结束 ${timeDiff}s`);
    },
  };
}
