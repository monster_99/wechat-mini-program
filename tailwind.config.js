/** @type {import('tailwindcss').Config} */
import tailwindColors from './src/utils/tailwindColors.js';
// 已2的倍数为单位递增，目前最大100
const baseSpacing = 2;
const defaultSpacingNum = new Array(50).fill(undefined);
const spacing = defaultSpacingNum.reduce((old, now, index) => {
  old[(index + 1) * baseSpacing] = `${baseSpacing * (index + 1)}rpx`;
  return old;
}, {});

module.exports = {
  prefix: 'lh-',
  content: ['./index.html', './src/**/*.{html,js,ts,jsx,tsx,vue}'],
  theme: {
    extend: {
      spacing,
      fontSize: spacing,
      lineHeight: spacing,
      colors: tailwindColors,
    },
  },
  plugins: [],
  corePlugins: {
    preflight: false,
  },
};
