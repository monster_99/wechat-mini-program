import inquirer from 'inquirer';
import packageJson from '../package.json' assert { type: 'json' }; // node16+

import { execSync } from 'child_process';

export const modeList = [
  {
    value: 1, // 机器人编号也用的这个字段
    name: '体验版（测试环境变量），不可以用于上线',
    cli: 'npm run build:mp-weixin:trialdev',
  },
  { value: 2, name: '体验版（正式环境变量）', cli: 'npm run build:mp-weixin' },
];

const quire = async () => {
  const userName = await execSync('git config user.name')
    .toString('utf-8')
    .trim();

  // 命令行的执行逻辑代码
  return inquirer
    .prompt([
      {
        type: 'list',
        name: 'mode',
        choices: [
          { value: 1, name: '体验版（测试环境变量），不可以用于上线' },
          { value: 2, name: '体验版（正式环境变量）' },
        ],
        message: '请选择打包模式',
      },
      {
        type: 'input',
        name: 'version',
        message: '请输入版本号',
        default: packageJson.version,
      },
      {
        type: 'input',
        name: 'user',
        message: '请输入上传用户信息',
        default: userName,
      },
    ])
    .then((answer) => {
      return answer;
    });
};

export default quire;
