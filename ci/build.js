import path from 'path';
import { fileURLToPath } from 'url';
import ora from 'ora';
import { execSync } from 'child_process';

const __filenameNew = fileURLToPath(import.meta.url);

export default async (modeItem) => {
  const spinner = ora({
    text: `正在打包`,
    color: 'green',
  });
  spinner.start();
  const cliSuccess = execSync(modeItem.cli, {
    cwd: path.resolve(__filenameNew, '../../'),
  }).toString('utf-8');
  spinner.succeed('打包成功');
};
