import path from 'path';
import { fileURLToPath } from 'url';
import quire, { modeList } from './quire.js';
import build from './build.js';
import upload from './upload.js';
import ora from 'ora';
import { execSync, exec } from 'child_process';

const __filenameNew = fileURLToPath(import.meta.url);

const cliSuccess = execSync('git status -s', {
  cwd: path.resolve(__filenameNew, '../../'),
}).toString('utf-8');

if (!!cliSuccess) {
  throw new Error('有未提交的代码');
}

quire().then(async (res) => {
  const { mode, version, user } = res;
  const modeItem = modeList.find((item) => item.value === mode);

  await build(modeItem);
  await upload(modeItem, version, user);

  // 还需要加一个log

  ora().start().succeed('上传成功');
});
